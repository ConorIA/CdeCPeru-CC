Circulación Atmosférica
========================================================
author: Conor I. Anderson
date: 22 de julio del 2017
autosize: true


¿Qué trae el viento?
========================================================
type: prompt

## <center><br><br><br><middle>pensar - 	emparejar - compartir </center>

¿Qué trae el viento?
========================================================

- En el siglo 17, Halley, un astrónomo / geofísico / matemático / meteorólogo / físico inglés, realizó una expedición a la isla de Santa Helena. 
- Hablando con marineros, llegó a definir los padrones de los vientos alisios
- Estableció el calentamiento del sol cómo motor de los vientos; y llego a identificar la presión como un control de la altitud to los vientos.
- Sugirió que la dirección del viento es por el movimiento del sol "mientras da la vuelta al mundo"

Halley
========================================================

<center>![halley](2. circulacion-figure/halley.png)</center>

<div style="FONT-SIZE:13px; LINE-HEIGHT:20px;  text-indent: -30px;">Halley, E. (1753). An Historical Account of the Trade Winds, and Monsoons, Observable in the Seas between and Near the Tropicks, with an Attempt to Assign the Phisical Cause of the Said Winds, By E. Halley. <i>Philosophical Transactions, 16</i>(179-191), 153-168, accessed July 15, 2017 from http://rstl.royalsocietypublishing.org/</div>

Hadley
========================================================

- Hadley era abogado y meteorólogo amateur inglés
- En 1735, público su teoría de la circulación atmosférica
  - Acertaba que el sol no podía set el único motor, pues el viento debería soplar del ecuador directo hacía los polos (de calor a frío)
  - Propuso la existencia de una célula
    - El aire sube en el ecuador, se desplaza hacia los polos, y hunde en los polo


La célula de Hadley
========================================================

- Hadley también propuso que la rotación de la tierra era el mecanismo de la direccionalidad de los vientos.
- Pero en este caso, el viento se desplazaría del Ecuador hacia el noreste y sureste hasta llegar a los polos.
  - El viento en los latitudes medios va del oeste al este. ¿Cuál es el motor?

--- 

<center>![one-cell](2. circulacion-figure/sing_cell.svg)</center>

<div style="FONT-SIZE:13px; LINE-HEIGHT:20px;  text-indent: -30px;"><b>Imagen:</b> Adaptado de Burschik y DWindrim [CC-BY-SA-3.0], via Wikipedia Commons</div>
<div style="FONT-SIZE:13px; LINE-HEIGHT:20px;  text-indent: -30px;">Hadley, G. (1735). Concerning the cause of the general trade-winds: By Geo. Hadley, Esq; FRS. <i>Philosophical Transactions, 39</i>(436-444), 58-62, accessed July 15, 2017 from http://rstl.royalsocietypublishing.org/.</div>

Ferrel
========================================================

- La teoría de Hadley tenía un error criticó: él pensó que el ímpetu linear del aire se conserva. 
- Ferrel, un meteorólogo de los EEUU, identificó que es el ímpetu angular que se conserva. 
- Mientras viaja el aire, llega a curvarse alrededor de los 30° N/S antes de llegar al polo
- Identificó diferencias en la velocidad de rotación entre la atmósfera y la superficie
  - El movimiento de aire con moción hacia el este está fortalecida
  - El movimiento de aire con moción hacía el oeste está debilitada
- Estas diferencias de aire causan acumulación de masas de aire a lo largo de la distancia entre ecuador y polo. (frentes)


Ferrel
========================================================

![Ferrel](https://upload.wikimedia.org/wikisource/en/7/78/Ferrel_nashville_56.png)

<div style="FONT-SIZE:13px; LINE-HEIGHT:20px;  text-indent: -30px;">Ferrel, W. (1856). <i>An Essay on the Winds and the Currents of the Ocean</i>. Cameron & Fall, Book and Job Printers, accessed July 15, 2017 from https://en.wikisource.org/wiki/An_essay_on_the_winds_and_the_currents_of_the_ocean</div>

Teoría actual: 3 celdas
========================================================

- Hadley (0-30 °N/S)
  - Aire sube en 0° y se desplaza hacía 30° donde hunde; vientos alisos
- Ferrel (30-60 °N/S)
  - Aire frío sube y aire tibio baja por gradientes de presión; viento del oeste
- Polar (60-90° N/S)
  - Aire tibio sube y baja en el polo;viento polar del este
  
---

<center>![three-cell](2. circulacion-figure/three_cell.svg)</center>
<div style="FONT-SIZE:13px; LINE-HEIGHT:20px;"><b>Imagen:</b> Adaptado de Burschik y DWindrim [CC-BY-SA-3.0], via Wikipedia Commons</div>

Vientos principales
========================================================

![winds](2. circulacion-figure/three_cell_labels.svg)
<div style="FONT-SIZE:13px; LINE-HEIGHT:20px;"><b>Imagen:</b> Adaptado de Burschik y DWindrim [CC-BY-SA-3.0], via Wikipedia Commons</div>

Vientos principales
========================================================
- Zona de Convergencia Intertropical (ZCIT) - zona dónde aire sube cerca al Ecuador. Baja presión; mucha precipitación.
- Vientos alisos: Vientes del nor/sur-este. Componente superficial de la célula Hadley.
- Altos subtropicales - zona de subsidencia de aire a 30°; alta presión, poca precipitación; también conocido como Latitudes del Caballo
- Vientos del Oeste - vientes que soplan hacía el nor(sur)-este. Componente superficial de la célula Ferrel. 
- Frente Polar - división entre el aire polar y aire templado a 60°
- Vientos polares - vientos del nor(sur)-este hacia los latitudes medios

Quiz
========================================================
type: quiz-multichoice

¿Cuál de las células se caracteriza de aire tibio?
- Ferrel [*]
- Hadley
- Polar

Quiz
========================================================
type: quiz-multichoice

¿Hacia que sentido se sopla los vientos polares?
- hacía el oeste
- hacía el norte
- hacía el sur
- todas las opciones son correctas [*]

Quiz
========================================================
type: quiz-multichoice

¿Las células de circulación son?
- Hadley, Pharrel, Polera
- Headley, Farrel, Polar
- Hadleigh, Furrel, Pholer
- ninguna de las opciones es correcta [*]

Quiz
========================================================
type: quiz-multichoice

¿Qué viento trato de explicar Halley?
- Vientos del norte
- Vientos del oeste
- Vientos alisos [*]
- Vientos aislados

Corrientes en Chorro
========================================================

- Los corrientes en chorro son vientos de miles de km de largo, cientos de km de ancho y unos km de grosor.
- Corren de 10 a 15 km sobre la superficie a una velocidad de entre 150 a 300 km/h
- Ocurren entre las células

Corrientes en Chorro
========================================================
<div style="FONT-SIZE:13px; LINE-HEIGHT:20px;"><b>Imagen:</b> Elaboración propia basada en figura de Brookes/Cole Publishing</div>


Corriente en Chorro Polar
========================================================
- Dónde el aire tibio de los latitudes medios choca con aire frío de los polos, existe una fuerte gradiente de presión
- Esa gradiente genera fuertes vientes que corren paralelo las isobaras
  - Isobara: una línea que conecta dos pintos de presión equivalente
- Tiene una fuerte influencia en la temperatura de los latitudes medios... brr!

Corriente en Chorro Subtropical
========================================================
- El corriente en chorro subtropical forma por el principio de la conservación del ímpetu angular (recuérdense de Ferrel?)
  - Mientras el aire mueve del ecuador hacía los polos, el radio de la rotación disminuye
  - El viento agarre más velocidad para mantener el ímpetu angular
- Poca influencia sobre las condiciones terrestres (tropopausa más alta)

Ímpetu angula
========================================================

<center>$m$: masa; $v$: velocidad; $r$: radio<br></center>
<br>
$$
  \begin{aligned}
  mv_1r_1 &= mv_2r_2 \\
  \textrm{si}~r_1 &> r_2 \\ \textrm{entonces,}~v2 &> v1
  \end{aligned}
$$

Estaciones y Continentes: Enero
========================================================

<center><img src="2. circulacion-figure/pressure_jan.png" height="500"></center>
<div style="FONT-SIZE:13px; LINE-HEIGHT:20px;"><b>Imagen:</b> Physical Sciences Division, Earth System Research Laboratory, NOAA [Public Domain], via http://www.esrl.noaa.gov/psd/ </div>

Estaciones y Continentes: Julio
========================================================

<center><img src="2. circulacion-figure/pressure_jul.png" height="500"></center>
<div style="FONT-SIZE:13px; LINE-HEIGHT:20px;"><b>Imagen:</b> Physical Sciences Division, Earth System Research Laboratory, NOAA [Public Domain], via http://www.esrl.noaa.gov/psd/ </div>

Estaciones y Continentes
========================================================

<center><img src="2. circulacion-figure/pressure_animation.gif" height="500"></center>
<div style="FONT-SIZE:13px; LINE-HEIGHT:20px;"><b>Imagen:</b> Adaptado de Physical Sciences Division, Earth System Research Laboratory, NOAA [Public Domain], via http://www.esrl.noaa.gov/psd/</div>

Fin
========================================================
type: section