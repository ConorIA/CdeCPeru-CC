Classificacíon Climática Köppen‒Geiger
========================================================
author: Conor I. Anderson
date: 22 de julio del 2017
autosize: true

Introducción
========================================================
- La clasificación climática es un remanente de las teorías de determinismo ambiental
- Hay varias clasificaciones climáticas, entre las cuales la de Köppen sigue siendo muy popular
- Nace en el 1884, se actualiza en el 1918, 1936, y en 1954 y 1961 con Geiger
- También conocida cómo Clasificación Climática Köppen&ndash;Geiger

========================================================
type:section

## <center>Temperatura / Humedad / Caracteristica<br></center>
## <center>A B C D E / S W f m w s T F / a b c d h k n</center>

========================================================

<center>![koppen](3. koppen-figure/koppen.png)</center>


Tropical (A)
========================================================
Cada mes tiene una temperatura media superando los 18°C

 - f: ecuatorial; precipitación de 60mm a más cada mes
 - m: monzónico; el mes más seco ve menos de 60mm de lluvia, pero más del 4% de precipitación anual
 - w: con invierno seco; el mes más seco ve menos de 60mm de lluvia, y menos del 4% de precipitación anual
 - s: con verano seco; el mes más seco ve menos de 60mm de lluvia, y menos del 4% de precipitación anual
 
========================================================

<center>![zonea](3. koppen-figure/zonea.png)</center>

Seco (B)
========================================================
Clima con muy poca precipitación; la clasificación se calcula: 

1. multiplicar la temperatura promedia anual por 20
2. si al menos 70% de la precipitación anual ocurre en la primavera y verano; agregar 280, sino<br>
si 30-70% de la precipitación anual ocurre en la primavera y verano; agregar 140, sino<br>
entonces, menos de 30% de la precipitación anual ocurre en la primavera y verano; agregar 0
3. si la precipitación anual es menos de 50% del resultado, la clasificación es BW (árido)<br>
si la precipitación supera el 50% del resultado, la clasificación es BS (semiárido / semifrio)

Seco (B) - tipo R
========================================================
`T_a  = temperatura media anual`<br>
`P_pv = precipitación en primavera y verano`<br>
`P_a  = total de precipitación anual`<br>


```{r, eval = FALSE}
result <- T_a * 20
if (P_pv > (0.7 * P_a)) {
  result <- result + 280
} else if (P_pv > (0.3 * P_a) && P_pv <= (0.7 * P_a)) {
  result <- result + 140
} # Nota, no necesitamos el tercer caso

if (P_a < (0.5 * resultado)) {
  return("BW")
} else {
  return("BS")
}
```

Seco (BS / BW)
========================================================

- h: cálido
  - La temperatura media anual está por encima de los 18 °C<br><center>_o_</center>
  - El mes mas frió tiene temperatura media > 0 °C (EEUU)
- k: frío
  - La temperatura media anual está por debajo de los 18° C<br><center>_o_</center>
  - Al meno un mes tiene temperatura media < 0 °C (EEUU)
  
========================================================

<center>![zoneb](3. koppen-figure/zoneb.png)</center>

Templado / Mesotermal (C[fsw])
========================================================

Este clima se clasifica por un mes frío de entre 0 °C y 18 °C y al meno un mes superior a las 10°C.

- Humedad
  - s: la estación seca coincide con el periodo de temperaturas altas (verano seco)
  - w: la estación seca coincide con el periodo de temperaturas bajas (invierno seco)
  - f: no hay una estación seca marcada (precipitaciones todo el año)
  
Templado / Mesotermal (C[fsw][abc])
========================================================

- Temperatura
  - a: el mes más cálido supera los 22 °C (subtropical)
  - b: el mes más cálido no llega a los 22 °C; al menos 4 meses con temperaturas medias sobre los 10 °C (templado)
  - c: el mes más cálido no llega a los 22 °C; menos de 4 meses con temperaturas medias sobre los 10 °C (subpolar)

Clases de C
========================================================

- Cf
  - Cfa: Subtropical húmedo
  - Cfb: Oceánico (verano suave)
  - Cfc: Subpolar oceánico
- Cw
  - Cwa: Subtropical con invierno seco (verano cálido)
  - Cwb: Templado con invierno seco (verano suave)
  
---

  - Cwc: Subpolar oceánico con invierno seco
- Cs
  - Csa: Mediterráneo (verano cálido)
  - Csb: Oceánico mediterráneo (verano suave)
  - Csc: Subpolar oceánico con verano seco

========================================================

<center>![zonec](3. koppen-figure/zonec.png)</center>

Continental (D[fsw])
========================================================

El continental tiene temperaturas mensuales medias de menos de 0 °C y mayor de 10 °C

- Humedad:
  - s: la estación seca coincide con el periodo de temperaturas altas (verano seco)
  - w: la estación seca coincide con el periodo de temperaturas bajas (invierno seco)
  - f: no hay una estación seca marcada (precipitaciones todo el año)

Continental (D[fsw][abcd])
========================================================
  
- Temperatura
  - a: el mes más cálido supera los 22 °C; al menos 4 meses con temperaturas medias sobre los 10 °C (verano caliente)
  - b: el mes más cálido no llega a los 22 °C; al menos 4 meses con temperaturas medias sobre los 10 °C (verano tibio)
  - c: el mes más cálido no llega a los 22 °C; menos de 4 meses con temperaturas medias sobre los 10 °C; el mes más frío está por debajo de 0 °C pero es superior a -38°C (subpolar)
  - d: el mes más cálido no llega a los 22 °C; menos de 4 meses con temperaturas medias sobre los 10 °C; el mes más frío está por debajo de -38°C (extremadamente frío)

Clases de D
========================================================

- Df
  - Dfa: Continental humeada de verano caliente 
  - Dfb: Continental humeada de verano tibio 
  - Dfc: Subartico
  - Dfd: Subartico extremadamente frío
- Dw
  - Dwa: Continental con invierno seco y verano caliente
  - Dwb: Continental con invierno seco y verano tibio
  - Dwc: Subartico con invierno seco
  - Dwd: Subartico extremadamente frío con invierno seco

Clases de D (cont.)
========================================================

- Ds
  - Dsa: Continental con verano seco y caliente
  - Dsb: Continental con verano seco y tibio
  - Dsc: Subartico con verano seco
  - Dsd: Subartico extremadamente frío con verano seco

========================================================

<center>![zoned](3. koppen-figure/zoned.png)</center>

Frió (E)
========================================================

- ET: Tundra tibia; Todos los meses tienen temperaturas medias entre 0 y 10 °C
- ETf: Tundra fría; Al meno un mes tiene temperaturas por debajo de los 0 °C
- EF: Clima de Glaciares; invierno eterno; ningún mes supera los 0 °C

========================================================

<center>![zonee](3. koppen-figure/zonee.png)</center>

Zonas Köppen‒Geiger en el Perú
========================================================

<center><img src="3. koppen-figure/peru-political.png" height="600"></center>

Zonas climáticas siendo Senamhi
========================================================

Senamhi emplea la clasificación Thornthwaite en el Perú:
Visitemos: http://www.peruclima.pe/?p=mapa-climatico-del-peru

Fin
===
type: section