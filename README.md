# Materiales de la 1ra edición del Club "Impactos del Cambio Climático" de Clubes de Ciencia Perú

En este repositorio, se encuentra las presentaciones y los ejercicios desarrollados para el Club denominado "[Impactos del Cambio Climático](https://www.clubesdecienciaperu.org/copy-of-energiasolar)" realizado en la Universidad Tecnológica del Perú, en la Ciudad de Lima entre el 22 y 27 del 2017 como parte de la primera edición de Clubes de Ciencia Perú.

## Temas presentados

|                                 Tema                                 |                        Observaciones                       |
|:--------------------------------------------------------------------:|:----------------------------------------------------------:|
| Introducción y Conceptos Básicos                                    |                                                            |
| Circulación Atmosférica                                              |                                                            |
| Clasificación Climática Köppen‒Geiger                               |                                                            |
| Clima de América Tropical                                            | Falta poner atribución de imágenes antes de publicar      |
| Introducción al Cambio Climático                                     | Materiales de Eduardo Sánchez                              |
| Unidades de Exposición                                               | Referir también a Ejercicio Z para detección de tendencias |
| El uso de herramientas para el análisis de “Big Data”                | Materiales de Karla Calderón                               |
| Para empezaR: Introducción al R                                      |                                                            |
| Los pasos de una EICC                                                |                                                            |
| Visita al Senamhi                                                    | No hay materiales                                          |
| Los Modelos de Circulación General / Escenarios del Cambio Climático |                                                            |
| Estudio de Caso de la Amazonía                                       | No se presentó                                             |
| Descarga de datos de los MCG                                         |                                                            |
| Presentación de Ejemplos de EICC de Canadá (W. Gough vía Skype)      | Materiales de William Gough                                |


## Licencia

[![license](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

Excepto en el caso de algunos imágenes, esta obra está bajo una [Licencia Creative Commons Atribución-CompartirIgual 4.0 Internacional](http://creativecommons.org/licenses/by-sa/4.0/).
