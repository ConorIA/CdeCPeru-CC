## Como usar: 
# 1. Abrir en RStudio
# 2. Hacer clic en "Source"
# 3. Correr con la expresión que evalué tu unidad de exposición

# Ejemplos de expresiónes:
#
# Dias calientes:
# recalc_exp(dat, result, "ACCESS1-0", "Tmax", expression(valores > 30))
#
# Dias helados:
# recalc_exp(dat, result, "ACCESS1-0", "Tmax", expression(valores < 0)))
#
# Grade-Días de Enfriamiento:
# recalc_exp(dat, result, "ACCESS1-0", "Tmean", expression(ifelse(valores > 18, valores - 18, 0)))
#
# Grado-Días de Calefacción:
# recalc_exp(dat, result, "ACCESS1-0", "Tmean", expression(ifelse(valores < 18, 18 - valores, 0)))

require(dplyr)

recalc_exp <- function(dat, result, modelo, variable, expr) {
  df <- data.frame()
  for (period in grep("-", colnames(result), value = TRUE)[-1]) {
    for (escen in unique(result$Scenario)) {
      anomalia <- result %>% filter(Model == modelo & Scenario == escen)
      valores <- dat[[which(names(dat) == variable)]] + unlist(select(anomalia, period)[3])
      range <- unlist(strsplit(period, "-"))
      unidad <- sum(eval(expr, envir = dat), na.rm = TRUE) / length(seq(range[1]:range[2]))
      row <- c(period, escen, unidad)
      names(row) <- c("Periodo", "Escenario", "Unidad")
      df <- rbind(df, row, stringsAsFactors = FALSE)
      names(df) <- c("Periodo", "Escenario", "Unidad")
    }
  } 
  df$Unidad <- as.numeric(df$Unidad)
  df
}
